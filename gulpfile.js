var gulp = require('gulp'),
  connect = require('gulp-connect');
 
gulp.task('connect', function() {
  connect.server({
    livereload: true
  });
});


gulp.task('reload', function () {
  gulp.src('./src/**/*')
    .pipe(connect.reload());
});

gulp.task('serve', function() {

    browserSync.init({
        server: "./src"
    });

    gulp.watch("app/scss/*.scss", ['sass']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['connect', 'reload']);