import {AlertsService} from './alertsService';
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(AlertsService, EventAggregator)
export class App {
    constructor(alertsService, eventAggregator) {
        eventAggregator.publish('alerts', {type: 'success', message: 'App initialized'});
        this.alertsService = alertsService;
        this.heading = 'Welcome to Aurelia';
        this.firstName = undefined;
        this.lastName = undefined;
        this.users = [{firstName: 'Mike', lastName: 'Smith'},
        {firstName: 'John', lastName: 'Smith'}];
        this.editMode = false;
        this.editedIndex = undefined;
        setInterval( () => this.dateTime = new Date, 1000);
        this.date = '2016-01-13';
    }
    
    get fullName() {
        return '${this.firstName} ${this.lastName}';
    }
    
    edit(index) {
        this.backup = {
            firstName: this.users[index].firstName,
            lastName: this.users[index].lastName
        };
        this.editMode = true;
        this.editedIndex = index;
    }
    
    save() {
        this.editMode = false;
        this.editedIndex = undefined;
    }
    
    cancel() {
        this.users[this.editedIndex].firstName = this.backup.firstName;
        this.users[this.editedIndex].lastName = this.backup.lastName;
        this.editMode = false;
        this.editedIndex = undefined;
    }
    
    delete(index) {
        this.users.splice(index, 1);
    }
    
    add() {
        this.users.push({firstName: this.firstName, lastName: this.lastName});
        let message = 'User ${this.firstName} ${this.lastName} added successfully.';
        this.alertsService.addSuccessAlert(message);
    }
}
