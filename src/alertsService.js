import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';

@inject(EventAggregator)
export class AlertsService {
  constructor(eventAggregator) {
      this.alerts = [{type: 'success', message: 'You have a dreamed SUV'}];
      this.eventAggregator = eventAggregator;
      this.eventAggregator.subscribe('alerts', payload => {
          this.alerts.push(payload);
    });
  }  
  addWarningAlert(message) {
      this.alerts.push({
          type: 'warning',
          message: message
      });
  }
    addSuccessAlert(message) {
      this.alerts.push({
          type: 'success',
          message: message
      });
  }
}