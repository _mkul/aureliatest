import {bindable} from 'aurelia-framework';

export class SetDate {
  @bindable date = null;
}