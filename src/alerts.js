import {AlertsService} from './alertsService';
import {inject} from 'aurelia-framework';

@inject(AlertsService)

export class Alerts {
    constructor(alertsService) {
        this.alertsService = alertsService;
        this.alerts = alertsService.alerts;
        console.log(this.alerts);
    }
}